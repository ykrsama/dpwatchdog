For [darkshine-simulation](https://gitlab.com/yulei_zhang/darkshine-simulation). Automatically submit, monitor condor jobs by a daemon process.

1. Edit jobconf

2. Generate seed list file

   ```
   ./seed_list
   ```

   Related jobconf parameters:

   | Name    | Description           | Example |
   | ------- | --------------------- | ------- |
   | NUM     | Total number of seeds | 10000   |
   | RNDBYTE | byte of seed          | 4       |

3. Generate jobs

   ```	
   ./gen_jobs
   ```

   Related jobconf parameters:

   | Name      | Description | Example   |
   | --------- | ----------- | --------- |
   | SEED_FILE | seed file   | “\$\{DIR\}\$/seed_1E4.txt” |

4. Submit jobs using DPwatchdog

   ```
   ./DPwatchdog
   ```

   Wait for a minute to scan jobs.

   - Watch progress status:

     ```
     cat watchdog.log
     ```

   - Restart if DPwatchdog process accidentally stopped:

     ```
     ./DPwatchdog watchdog.log
     ```


   - Stop DPwatchdog:

     ```
     ps x | grep DPwatchdog.sh
     kill <PID>
     condor_rm -a
     ```

   Related jobconf parameters:

   | Name      | Description | Example   |
   | --------- | ----------- | --------- |
   | JOB_TYPE  | DP - DSimu; Ana - DAna; all - DSimu and DAna | DP |
   | DONE_FILE | file that indicate job done | DSimu_Done |
   | AUTO_MODE | true - Let Total jobs for all users to be MIN_JOBS. Then submit more jobs when there is  no idle jobs.<br>false - Always submit MIN_JOBS number of jobs for this user. | true |
   | MIN_JOBS | see AUTO_MODE | 700 |

5. Remove watchdog.log file

6. Merge root file

   ```
   cp merge_root jobconf <output_dir>
   cd <output_dir>
   ./merge_root
   ```


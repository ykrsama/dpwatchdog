#!/bin/bash

# Authored by Xuliang
# v1.0

# get startN, endN, JOBS_DIR, JOBS_N, MIN_TOTAL_JOBS
source jobconf

# init
startN=0
endN=0
# jobQuery=0
jobDone=0
Do_submit=false


check_condor()
{
    condor_out=`condor_q`
    allIdleJobs=`echo -e "${condor_out}" | tail -n1 | head -n1 | cut -d':' -f2| cut -d' ' -f8`
    allRunningJobs=`echo -e "${condor_out}" | tail -n1 | head -n1 | cut -d':' -f2| cut -d' ' -f10`
    allUserJobs=$((allIdleJobs + allRunningJobs))
    jobIdle=`echo -e "${condor_out}" | tail -n2 | head -n1|cut -d',' -f3|cut -d' ' -f2`
    myJobs=`echo -e "${condor_out}" | tail -n2 | head -n1|cut -d';' -f1|cut -d' ' -f4`
    
    if $AUTO_MODE
    then
        jobStep=$((MIN_JOBS - allUserJobs - 1))
        if [[ ${allUserJobs} -lt ${MIN_JOBS} ]]
        then
            Do_submit=true
        else
            Do_submit=false
        fi
    else
        jobStep=$((MIN_JOBS - myJobs - 1))
        if [[ ${myJobs} -lt ${MIN_JOBS} ]]
        then
            Do_submit=true
        else
            Do_submit=false
        fi
    fi
}


if [ $# -eq 1 ]
then
    # Get startN
    source $1

    if [ -z $Progress ]
    then
        echo "Invalid input"
        exit
    else
        echo "continue monitoring jobs from ${startN}"
    fi
elif [ -f watchdog.log ]
then
    echo -e "File watchdog.log exist. Remove it to restart jobs, or continue by:\nsetsid ./DPwatchdog watchdog.log > watchdog.out 2>&1 "
    exit
fi

while [ $jobDone -lt $JOBS_N ]
do
    source jobconf
    jobDone=0
    # Scan job status
    for dire in `ls ${JOBS_DIR}`
    do
        if [[ -f ${JOBS_DIR}/${dire}/${DONE_FILE} ]]
        then
            # sum up the total number of done jobs
            jobDone=$((jobDone + 1))
        else
            cur=${dire:1}
            logFile=${JOBS_DIR}/${dire}/log/sim${cur}.log
            if [ -f $logFile ]
            then
                terminatedLine=`grep "Job terminated" ${logFile} | head -n1`
                abortedLine=`grep "Job aborted" ${logFile} | head -n1`
                evictedLine=`grep "Job was evicted" ${logFile} | head -n1`
            else
                unset terminatedLine
                unset abortedLine
                unset evictedLine
            fi
            
            check_condor
            if [[ ( ! -f ${JOBS_DIR}/${dire}/${DONE_FILE} ) && ${Do_submit} == true ]] # check again
            then
                # If terminated, check DONE_FILE. If not done, resubmit.
                if [[ ! -z ${terminatedLine} ]]
                then
                    echo "[ Warning ] j$cur failed, restarting."
                    rm $logFile
                    ./submit ${JOB_TYPE} $cur $cur
                elif [[ ! -z ${abortedLine} ]]
                then
                    echo "[ Warning ] j$cur aborted, restarting."
                    rm $logFile
                    ./submit ${JOB_TYPE} $cur $cur
                # If evicted, remove job and resubmit.
                elif [[ ! -z ${evictedLine} ]]
                then
                    echo "[ Warning ] j$cur evicted, restarting."
                    # Get job ID and remove job
                    jobID=${evictedLine#* (} # cut prev string
                    jobID=${jobID%%.*} # cut post string
                    condor_rm jobID
                    rm $logFile
                    ./submit ${JOB_TYPE} $cur $cur
                fi
            fi 
        fi
    #sleep 0.001
    done

    # Submit new job
    check_condor
    if [ $startN -lt $JOBS_N ] 
    then
        if $Do_submit
        then
            startN=$((endN + 1))
            endN=$((startN + jobStep))
            ./submit ${JOB_TYPE} $startN $endN
        elif [ $jobIdle -eq 0 ]
        then
            if $AUTO_MODE
            then
                startN=$((endN + 1))
                endN=$startN
                ./submit ${JOB_TYPE} $startN $endN
            fi
        fi
    fi
    # save progress
    echo -e "startN=${startN}\nendN=${endN}\nProgress=${jobDone}/${JOBS_N}" > watchdog.log
   
    #sleep 3
done
